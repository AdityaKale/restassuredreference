package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class Patch_Request_Repository extends Endpoint3 {

	public static String Patch_Tc1_Request() throws IOException {
		
		 ArrayList<String> exceldata =Excel_Data_Reader.Read_Excel_Data("Api_Data.xlsx",
					"Patch_Api","Patch_TC_1");
		 
		 String req_name=exceldata.get(1);
			String req_job=exceldata.get(2);
		
		String requestbody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		
		
		return requestbody;
		

	}

}
