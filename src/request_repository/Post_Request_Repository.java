package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class Post_Request_Repository extends Endpoints {

	public static String Post_Tc1_Request () throws IOException {
		
		ArrayList <String> exceldata =Excel_Data_Reader.Read_Excel_Data("Api_Data.xlsx",
				"Post_Api","Post_TC_1 ");
		
		//System.out.println(exceldata);
		String req_name=exceldata.get(1);
		String req_job=exceldata.get(2);
		
		String requestbody="{\n"
				+ "    \"name\": \""+req_name+"\",\n"
				+ "    \"job\": \""+req_job+"\"\n"
				+ "}";
		return requestbody;
		
		

	}

}
