package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class Put_Request_Respository extends Endpoint2 {

	public static String Put_TC1_request() throws IOException {
		
		
ArrayList<String> exceldata =Excel_Data_Reader.Read_Excel_Data("Api_Data.xlsx",
		"Put_Api","Put_TC_1");
		
		//System.out.println(exceldata);
		String req_name=exceldata.get(1);
		String req_job=exceldata.get(2);
		
		 
		String requestbody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		
		return requestbody;
		

	}

}
