package common_method_package;

import static io.restassured.RestAssured.given;



public class Triggered_api_method {

	public static int extract_status_code(String req_Body,String URL) {
		 
		int statuscode=given().header("Content-Type","application/json")
				.body(req_Body)
				.when().post(URL)
				.then().extract().statusCode();
		return statuscode;
	}
		
		
	 public static String extract_response_body(String req_Body,String URL)
	 {			
		 String responsebody=given()
					.header("Content-Type","application/json")
					.body(req_Body)
					.when().post(URL)
					.then().extract().response().asString();
			return responsebody;
				
		

		
		
	}
	
}
