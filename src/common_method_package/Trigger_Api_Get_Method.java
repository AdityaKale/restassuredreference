package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Endpoint1;

public class Trigger_Api_Get_Method extends Endpoint1 {

	public static int extract_status_code(String URL){
	  
		int statuscode=given().header("Content-Type","application/json")
				.when().get(URL).then().extract().statusCode();
		return statuscode;
		
	}
	public static String extract_response_body(String URL){
		  
		String responsebody=given().header("Content-Type","application/json")
				.when().get(URL)
				.then().extract().response().asString();
		System.out.println(responsebody);
		return responsebody;

	}

}
