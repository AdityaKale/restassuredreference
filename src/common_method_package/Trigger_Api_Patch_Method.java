package common_method_package;

import static io.restassured.RestAssured.given;

public class Trigger_Api_Patch_Method {

public static int extract_status_code(String req_Body,String URL) {
		 
		int statuscode=given().header("Content-Type","application/json")
				.body(req_Body)
				.when().patch(URL)
				.then().extract().statusCode();
		return statuscode;
	}
		
		
	 public static String extract_response_body(String req_Body,String URL)
	 {			
		 String responsebody=given()
					.header("Content-Type","application/json")
					.body(req_Body)
					.when().patch(URL)
					.then().extract().response().asString();
			return responsebody;
				

}

}




