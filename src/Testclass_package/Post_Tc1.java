  package Testclass_package;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Triggered_api_method;
import common_utility_package.Handle_Api_Logs;
import io.restassured.path.json.JsonPath;
import request_repository.Post_Request_Repository;

public class Post_Tc1 extends Post_Request_Repository {
	@Test

	public static void exectour() throws IOException {
		
		File Directory_Name=Handle_Api_Logs.Create_Log_Directory("Post_Tc1");
		String requestbody=Post_Tc1_Request();
		
		
		for (int i=0;i<5;i++) {
			int statuscode=Triggered_api_method
		.extract_status_code(requestbody, post_endpoint());
		  
			//System.out.println(statuscode);
			
			if(statuscode == 201) {
				 String responsebody=Triggered_api_method.
		 extract_response_body(requestbody, post_endpoint());
		 	System.out.println(responsebody);
		 Handle_Api_Logs.evidence_creator(Directory_Name, "Post_Tc1",post_endpoint(),
				Post_Tc1_Request (), responsebody);
		validator(requestbody,responsebody);	
		 	break;
			
			}
			else {
				System.out.println("desired status code not found,hence retry");
			}
		}
		
		
	}
	
	public static void validator (String  req_body,String responsebody) 
	{

		//reqbody
		
        JsonPath jsp_req=new JsonPath(req_body);
		
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		
		//responsebody

	JsonPath jsp=new JsonPath(responsebody);
	 String res_name=jsp.getString("name");
     String res_job=jsp.getString("job");
     
     
     
     //validation 
     
     Assert.assertEquals(req_name,res_name);
     Assert.assertEquals(req_job,res_job);
     
	}

}
