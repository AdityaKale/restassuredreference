package Testclass_package;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Triggered_api_method;
import common_utility_package.Handle_Api_Logs;
import io.restassured.path.json.JsonPath;
import request_repository.Put_Request_Respository;

public class Put_Tc1 extends Put_Request_Respository {
		
    @Test
	public static void exectour() throws IOException {
		
		String req_body=Put_TC1_request();
		
		File Directory_Name=Handle_Api_Logs.Create_Log_Directory("Put_Tc1");
		for(int i=0;i<5;i++) {
		int statuscode=Triggered_api_method
	.extract_status_code( req_body, put_endpoint());
		System.out.println(statuscode);
		
		if(statuscode == 201) {
			 String responsebody=Triggered_api_method.
	 extract_response_body( req_body, put_endpoint());
	 	System.out.println(responsebody);
	 	 Handle_Api_Logs.evidence_creator(Directory_Name, "Put_Tc1",  put_endpoint(),
	 			 req_body, responsebody);
		validator( req_body,responsebody);
		break;
		}
		else {
			System.out.println("desired status code not found hence retry");
		}
	}
		}
		public static void validator( String  req_body,String responsebody)
		{
              JsonPath jsp_req=new JsonPath(req_body);
			
			String req_name=jsp_req.getString("name");
			String req_job=jsp_req.getString("job");
			
			
			
			
			JsonPath jsp_res=new JsonPath(responsebody);
			
			String res_name=jsp_res.getString("name");
			String res_job=jsp_res.getString("job");
			
			
			//validation
			
			Assert.assertEquals(res_name,"morpheus");
			Assert.assertEquals(res_job, "zion resident");
			
			
		}
			
			
			
			
			
}

	
	
	
