

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class Dynamic_Driver_Class {

	public static void main(String[] args) throws IOException, 
	
	ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException,
	
	IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		//Step 1-Read the test cases to be executed from the excel file
		
	ArrayList <String> TestCaseList	= Excel_Data_Reader.Read_Excel_Data("Api_Data.xlsx",
			"TestCasesToExecute", "TestCaseToExecute");
		 System.out.println(TestCaseList);
		int count=TestCaseList.size();
		
		for (int i =1;i<count;i++) {
			String TestCaseToExecute=TestCaseList.get(i);
			System.out.println( "Test case which is going to execute:" + TestCaseToExecute);
			
			//step 2- call the TestCaseToExecute on runtime by using java.lang.reflectpackage
			
		Class  <?> TestClass=Class.forName("Testclass_package."+TestCaseToExecute);
			
		// step 3 = call the class capture in the var in Test class by using java.lang.reflectmethod
		
		Method ExecuteMethod=TestClass.getDeclaredMethod("exectour");

		
			// step 4= Set the accessibility of the method as true 
		
		ExecuteMethod.setAccessible(true);
		
		//step 5= create the instance of a class captured in a test class var
		
  Object InstanceOfTestClass=TestClass.getDeclaredConstructor().newInstance();
		
		//step 6= execute the method capture in var ExecuteMethod capture in TestClass var

               ExecuteMethod.invoke(InstanceOfTestClass);
                System.out.println(":::::::::::::::::::::::::::::::::::");
                
                
          }
	
	}
}
