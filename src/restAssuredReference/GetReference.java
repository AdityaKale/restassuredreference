package restAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class GetReference {

	public static void main(String[] args) {
		// Step 1-declare the variable for BaseURI and Request body
		String BaseURI="https://reqres.in/";
		
		//step 2-BaseURI
		RestAssured.baseURI=BaseURI;		
        
		//step 3-trigger the api
		
		//String resbody=given().when().get("api/users?page=2").then().extract().response()
			//	.asString();
		//System.out.println(resbody);
		
		String resbody=given().log().all().when().get("api/users?page=2").then().log().all().extract()
		.response().asString();
		System.out.println(resbody);

	// create An Object of Json Path To Parse The resbody
		
		JsonPath jsp_res=new JsonPath(resbody);
		
		int res_id=jsp_res.getInt("data.id");
		System.out.println(res_id);
		
		String res_email=jsp_res.getString("data.email");
		System.out.println(res_email);

		String res_first_name=jsp_res.getString("data.first_name");
		System.out.println(res_first_name);
		
		String res_last_name=jsp_res.getString("data.last_name");
		System.out.println(res_last_name);
		
		String res_avatar=jsp_res.getString("data.avatar");
		System.out.println(res_avatar);
		
		//validation
		
		Assert.assertEquals(res_id, 7);
		Assert.assertEquals(res_email, "michael.lawson@reqres.in");
		Assert.assertEquals(res_first_name, "Michael");
		Assert.assertEquals(res_last_name, "Lawson");
	    Assert.assertEquals(res_avatar, "https://reqres.in/img/faces/7-image.jpg");
	}

}
