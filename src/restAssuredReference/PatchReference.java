package restAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class PatchReference {

	public static void main(String[] args) {
		
		// Step1- declare the BaseURI and requestBody
		
		String BaseURI="https://reqres.in/";
		String RequestBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
       
		//step 2 - baseURI
		RestAssured.baseURI=BaseURI;
		
		//step 3- trigger the api
		//String resbody=given().header("Content-Type","application/json").body(RequestBody)
			//	.when().patch("api/users/2").then().extract().response().asString();
		//System.out.println(resbody);
		
		String resbody=given().header("Content-Type","application/json").body(RequestBody).log().all()
		.when().patch("api/users/2")
        .then().log().all().extract().response().asString();		
		
		// create an object of json path to parse the resbody
		JsonPath jsp_res=new JsonPath(resbody);
		
		String res_name=jsp_res.getString("name");
		System.out.println("res_name");
		
		String res_job=jsp_res.getString("job");
		System.out.println("res_job");
		
		String res_updatedAt=jsp_res.getString("updatedAt");
		
		String res_newDate=res_updatedAt.substring(0,10);
		System.out.println("new_Date");
		
		LocalDateTime Date=LocalDateTime.now();
		String newDate=Date.toString();
		
		String currentDate=newDate.substring(0,10);
		System.out.println("currentDate");
		
		//create an object of json path to parse the reqbody
		
		JsonPath jsp_req=new JsonPath(RequestBody);
		
		String req_name=jsp_req.getString("name");
		System.out.println("req_name");
		
		String req_job=jsp_req.getString("job");
		System.out.println("req_job");
		
		//validate the response body parameter (testng)

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(currentDate,newDate);

}
}