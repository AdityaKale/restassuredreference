package restAssuredReference;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;

public class DelReference {

	public static void main(String[] args) {
		// step 1= declare the variable baseURI
		String BaseURI="https://reqres.in/";
		
		//step2-BaseURI
		RestAssured.baseURI=BaseURI;
		
		// step 3= trigger the api
		given().header("Content-Type","application/json").log().all().when().delete("api/users/2").then().log()
		.all().extract().response().asString() ;
		

	}

}
