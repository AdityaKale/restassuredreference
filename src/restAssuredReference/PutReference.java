package restAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class PutReference {

	public static void main(String[] args) {
		// step 1-declare the baseURI and RequestBody
		String BaseURI="https://reqres.in/";
		String Requestbody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		//step 2- Create an obj of json path to parse the requestbody
		JsonPath jsp_req=new JsonPath(Requestbody);
		
		// step 3:extract the values from requestbody
		String req_name=jsp_req.getString("name");
		System.out.println("Requestbody parameter 1:" +req_name);
		String req_job=jsp_req.getString("job");
		System.out.println("Requestbody parameter 2:" +req_job);
		
		//step 4- declare the basURI
		RestAssured.baseURI=BaseURI;
		
		//step 5 - Configure the requestbody & trigger the requestbody
		
         
		 String Responsebody=given().header("Content-Type","application/json").body(Requestbody).log().all()
		.when().put("api/users/2").then().log().all().extract().response().asString();	
		 
		 //step 6 - create an object of jsonpath to parse the responsebody
		 
		 JsonPath jsp_res=new JsonPath(Responsebody);
		 
		 //Extract the value to parse the responsebody
		    String res_name=jsp_req.getString("name");
			System.out.println("Responsebody parameter 1:" +res_name);
			String res_job=jsp_req.getString("job");
			System.out.println("Responsebody parameter 2:" +res_job);
			
		//step 8 - validate the responsebody parameter
			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, res_job);
			
		
		 
		 
		 
		 

		 
		
		
	}

}
