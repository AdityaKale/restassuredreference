package restAssuredReference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class PostReference {

	public static void main(String[] args) {
		//step 1:declare the variable for BaseURI and Request body
		String BaseURI="https://reqres.in/";
		String Requestbody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}"; 
		//step 2:BaseURI
		RestAssured.baseURI=BaseURI;
		
		//step 3:Configure Request body trigger the body
		/*String responsebody=given().header("Content-Type","application/json")
		 * .body(Requestbody).when().post("api/users")
		.then().extract().response().asString();
		System.out.println(responsebody);*/
		
		String responsebody=given().header("Content-Type","application/json")
				.body(Requestbody).log().all()
		.when().post("api/users").then().log().all().extract().asString();
      
		//step 4- create an object of jsonpath to parse the parse the responsebody
		
		JsonPath jsp_res=new JsonPath(responsebody);
		
		String res_name=jsp_res.getString("name");
		System.out.println("Responsebody parameter 1:"+ res_name);
		
		String res_job=jsp_res.getString("job");
		System.out.println("Responsebody parameter 1:"+ res_job);
		
		String res_createdAt=jsp_res.getString("createdAt");
		
		String res_generatedDate=res_createdAt.substring(0,10);
		System.out.println("generatedDate");
		
		LocalDateTime currentDate=LocalDateTime.now();
		
		String newDate=currentDate.toString();
		String updatedDate=newDate.substring(0,10);
		
		System.out.println(updatedDate);
		
		JsonPath jsp_req=new JsonPath(Requestbody);
		
		String req_name=jsp_req.getString("name");
		System.out.println("Requestbody parameter :" +req_name);
		
		String req_job=jsp_req.getString("job");
		System.out.println("Requestbody parameter :" +req_job);
		
		  
		
		//step 6= validate the responsebody parameter
		
		Assert.assertEquals(req_name, req_name);
		Assert.assertEquals(req_job, res_job);
	 	Assert.assertEquals(newDate,updatedDate);
	
	}

}
