package common_utility_package;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.formula.functions.Rows;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Reader {

	 
			
			public static ArrayList <String> Read_Excel_Data(String File_Name,String Sheet_Name,
					String Test_Case_Name) throws IOException{
				
				ArrayList <String> ArrayData = new ArrayList <String>();
				// step 1 - Locate the File
			String Project_Dir	=System.getProperty("user.dir");
			FileInputStream fis=new FileInputStream(Project_Dir + "\\Input_Data\\" + File_Name);
				
				// step 2- access the located excel file
			XSSFWorkbook wb=new XSSFWorkbook(fis);
			
		        // step 3- count the no of sheets in Excel file
			   
			int countofsheet =wb.getNumberOfSheets();
			System.out.println(countofsheet);
			
			// step 4- Access the desired sheet
			for (int i=0;i< countofsheet;i++) {
				String sheetname=wb.getSheetName(i);
				if(sheetname.equals(Sheet_Name)) {
					System.out.println("inside the sheet :" + sheetname);
					XSSFSheet sheet=wb.getSheetAt(i);
				Iterator<Row> Rows=sheet.iterator();
				while (Rows.hasNext()) {
					Row currentRow=Rows.next();
					
					//step 5-Access the row corresponding the desired test case
					
					if(currentRow.getCell(0).getStringCellValue().equals(Test_Case_Name)) {
					
					Iterator<Cell> Cell = currentRow.iterator();
					while(Cell.hasNext()) {
						
					String Data=Cell.next().getStringCellValue();
					//System.out.println(Data);
						ArrayData.add(Data);
					}
				}
			}
		}
			
   }
			wb.close();
		return ArrayData;		
				
			}

		}

