package common_utility_package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_Api_Logs {
	public static File Create_Log_Directory (String Directory_Name) {
  String Current_Project_Directory=System.getProperty("user.dir");
  System.out.println( Current_Project_Directory);
  File Log_Directory=new File(Current_Project_Directory +"//Api_Logs//" +Directory_Name);
  Delete_Directory(Log_Directory);
  Log_Directory.mkdir();
		return  Log_Directory;
	}
	
	public static boolean Delete_Directory (File Directory) {
		
		boolean Directory_deleted=Directory.delete();
		
		if(Directory.exists()) {
			
		File[] files=Directory.listFiles();
		
		if(files != null) {
			for (File file:files) {
				
				if (file.isDirectory()) {
					
					Delete_Directory(file);
					
				}else {
					 file.delete();	
				}
			}
		} Directory_deleted= Directory.delete();
 }
		return Directory_deleted;
 }
	
	public static void evidence_creator(File Directory,String filename,String endpoint,
				String requestbody,String responsebody) throws IOException
{
		// step1=create file at given location
		
		File newfile=new File(Directory +"\\"+ filename +".txt");
		System.out.println("new file created to save evidence:" + newfile.getName());
		
		// Step 2- write data into the file
		
		FileWriter datawriter=new FileWriter(newfile);
		datawriter.write("End point is: "+ endpoint +"\n\n");
		datawriter.write( "Request body: \n\n" +requestbody+"\n\n");
        datawriter.write("Response body: \n\n" +responsebody); 
        datawriter.close();
        System.out.println("evidence is written in file:"+ newfile.getName());
        
}
 
}
