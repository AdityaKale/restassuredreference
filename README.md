In this project, we've created a framework to easily test APIs.
It provides a convenient and expressive way to send HTTP requests and validate API responses
key components:
Endpoints.java: This class defines the target website or API endpoints and specifies the various requests your tests will make. It serves as a central repository for endpoint details.
Request_Repository.java: Responsible for creating and managing test data, as well as reading data from Excel files. It streamlines the process of preparing data for your tests, enhancing reusability.
Trigger Api Method.java: This class is in charge of sending HTTP requests and handling responses with proper formatting, typically utilizing RestAssured. It encapsulates the core API interaction logic.
Testclass.java: Represents a specific test case and is responsible for executing it. It utilizes methods from TriggerPostApiMethod to send requests, validate responses, and ensure the test case's correctness.
Handle_Api_Logs.java: Manages the storage of test data, including logs or other relevant information. Proper logging is essential for debugging and monitoring test results.
ExcelDataReader.java: Reads data from Excel files, making it a structured and flexible way to manage test data. This aids in data-driven testing.
DriverClass.java: Serves as the entry point for test execution. It allows you to select which type of test to run, such as POST or GET requests. This component offers control over the test process.
